# Atreyee Ghosal: Resume

This is a summary of the work I've done and any important achievements.

## Github 

https://github.com/AdLucem

## Education

- *Current :* Dual Degree in CS and Computational Linguistics at International Institute of Information Technology, Hyderabad, India


## Key Projects

### GRU Implementation

Implementation of a Gated Recurrent Cell in both Haskell and Python. Part of a larger set of open-source contributions to dataHaskell and Hasktorch- functional data science libraries. Link: https://github.com/AdLucem/gru-encoder-decoder.git 


### NER for Chemical Domain

- A rule-mining system (written in Haskell) to infer human-interpretable rules from a POS-tagged training corpus. Link: https://github.com/AdLucem/rule-inference-code.git

- Using rules returned by the rule mining system, implementation of a Named Entity Recognition system for chemical research papers/documents.

## Experience

### L-3 Communications India 

*May 2018 - July 2018*
  
- Developed a semantic search engine for manuals for L-3's IPMS software.
- Built a module to take rough pdf data and convert it to a title-indexed set of documents. Link: https://github.com/AdLucem/pdf\_parser
- Built a custom search algorithm using word vectors and dependency parsing, that ranks the 'importance' of a word in a given sentence. The search engine code is not open-source.


### Virtual Labs, IIIT Hyderabad

*June 2017 - July 2017*
  
- Research Assistant: did a comparative study of Python and Haskell as languages.
- Built a basic web application in Haskell, mimicing the architecture of a Python webapp.
- Studied software metrics and applied metrics like SLOC and Cyclomatic Complexity to both the webapps.
- Link: https://github.com/vlead/web-app-haskell.git

## Other Projects

#### Mapping Cross-Lingual Word Embeddings
  
- An attempt to map words, using word embeddings, from one language to another, using two ML techniques- one unsupervised (an iterative version of nearest-neighbour) and one supervised (linear regression). Written in Python. Link: https://github.com/AdLucem/multilingual-word-embeddings

#### Dependency Parser in Haskell
  
- A Dependency Parser Framework that takes in a separate grammar module and sentences, and returns a set of parses. An implementation of a research paper. Link: https://github.com/AdLucem/Dependency-Parser-in-Haskell

#### A Basic Shell in C
  
- A basic shell that implemented common Linux shell commands like cd, ls, pipes, job manipulation, etc. Link: https://github.com/AdLucem/catboat

## Key Skills

- **Proficient In:** Python, C, C++, Haskell, Javascript, HTML, CSS, SQL
- **Basic Knowledge Of:** Elm, Java, Ruby, eLisp, Assembly (MIPS)
- **Web Frameworks:** Flask, node.js, Yesod (Haskell), Servant (Haskell)
- **Servers:** Apache, Nginx
- **Other Softwares:** OpenGL and WebGL (graphics), Pytorch and Tensorflow (Machine Learning), ATen, spaCy (NLP), Apache OpenNLP  


